<p align="center">
    <img src="https://images.gitee.com/uploads/images/2021/0206/165428_730dc581_1152471.png"/>
    <p align="center">
        一个最基础的 JWT + 拦截器 SDK！
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
        <img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
        <img src="https://img.shields.io/badge/license-MulanPSL-yellowgreen">
    </p>
</p>

---

# 项目简介

JI 即 jwt interceptor的缩写，项目起源的初衷是由于作者在个人项目和公司项目中有多处使用到 JWT 鉴权的场景，导致每个项目都写了大量 JWT 相关的重复代码，为了避免重复工作，故而将 **JWT + 拦截器** 部分代码抽取出来作为公共 SDK 给其他项目使用，也好达到统一升级的好处。

# 快速开始

## 1、克隆项目

```
git clone https://gitee.com/ok-tool/ji-spring-boot-starter.git
```

## 2、执行打包

```
mvn clean install -Dmaven.test.skip=true
```

## 3、引入依赖

```xml
<dependency>
    <groupId>cn.xlbweb</groupId>
    <artifactId>ji-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```

## 4、配置文件

```properties
# 开启JI组件（默认不开启）
cn.xlbweb.ji.enabled=true

# 白名单请求
cn.xlbweb.ji.exclude-uris=/**

# 前后端交互token名称
cn.xlbweb.ji.token-name=JiToken

# token有效时间（单位分钟，默认60分钟）
cn.xlbweb.ji.token-expiration-time=60
```

```properties
# token为空的响应码和响应信息
cn.xlbweb.ji.token-not-blank-code=1001
cn.xlbweb.ji.token-not-blank-message=Token不能为空

# token过期的响应码和响应信息
cn.xlbweb.ji.token-expired-code=1002
cn.xlbweb.ji.token-expired-message=Token已过期

# token验证失败的响应码和响应信息
cn.xlbweb.ji.token-invalid-code=1003
cn.xlbweb.ji.token-invalid-message=Token校验失败

# 用户无权限的响应码和响应信息
cn.xlbweb.ji.no-permission-code=1004
cn.xlbweb.ji.no-permission-message=用户无权限
```

# 常用方法

## 1、加密解密

```java
JwtUtils.encrypt("要加密的字符串")
JwtUtils.decrypt("要解密的字符串")

// 从请求头中获取token并解密
JwtUtils.decrypt();
```

## 2、账号&角色

```java
String accunt = JwtUtils.getAccunt();
String role = JwtUtils.getRole();
```

约定：为了正确的获取账号和角色信息，建议用户登录成功后生产的 token 串是由 **账号+角色** 组成，调用下面方法即可。

```java
JwtUtils.encryptBySpliceUserInfo(ACCOUNT, ROLE)
```

## 3、过期时间

```java
JwtUtils.getExpirationTime(token);

// 从请求头中获取token并解密获取过期时间
JwtUtils.getExpirationTime();
```

## 4、Subject

```java
JwtUtils.getSubject(token);

// 从请求头中获取token并解密获取getSubject
JwtUtils.getSubject();
```

## 5、权限（注解）

SDK 内置了两个角色（admin 和 manager），可在 Controller 层方法 api 上面加上 `@RequiresAdmin` 和 `@RequiresManager` 注解进行使用。

### @RequiresAdmin

```java
@GetMapping("/users/{id}")
// 需要超级管理员权限
@RequiresAdmin
public ServerResponse getUser(@PathVariable Integer id) {
    return userService.getUser(id);
}
```
### @RequiresManager

```java
@GetMapping("/users/{id}")
// 需要普通管理员权限
@RequiresManager
public ServerResponse getUser(@PathVariable Integer id) {
    return userService.getUser(id);
}
```

### 如何扩展注解

待补充...

## 6、权限（拦截器）

待补充...

# 关于返回

SDK 的所有返回信息均通过 json 形式返回，以下是几种常见返回信息， **可以通过配置文件对默认值进行修改**。

**注意：token 信息目前只支持前端放在http请求头中发送到后台，否则后台拿到的值为null。**

```json
{
    "code": 1001,
    "message": "Token不能为空"
}
```

- token 信息过期，验证不通过；

```json
{
    "code": 1002,
    "message": "Token已过期"
}
```

- token 信息不正确，如错误的 token 或瞎传 token，验证不通过；

```json
{
    "code": 1003,
    "message": "Token验证失败"
}
```

- 权限不够时；

```json
{
    "code": 1004,
    "message": "用户无权限"
}
```

# 使用样例

针对以上几点而言，为了更好的使用 SDK 的功能，以下为 JwtUtils 的常用功能和用户登录逻辑示例，方便大家参考。

```java
// 加密（账号和角色一起加密）
String token = JwtUtils.encryptBySpliceUserInfo("zhangsan", "ADMIN");
System.out.println(token);

// 解密
String parseResult = JwtUtils.decrypt(token);
System.out.println(parseResult);

// 账号，token会自动从request header中获取
String account = JwtUtils.getAccount();
System.out.println(account);

// 角色，token会自动从request header中获取
String role = JwtUtils.getRole();
System.out.println(role);
```

```java
@Override
public ResponseServer login(LoginDTO dto) {
    String md5Password = DigestUtils.md5DigestAsHex(dto.getPassword().getBytes());
    UserDO userDO = userRepository.findByUsernameAndPassword(dto.getAccount(), md5Password);
    if (Objects.nonNull(userDO)) {
        RoleDO roleDO = roleRepository.getOne(userDO.getRoleId());
        return ResponseServer.success("登录成功", JwtUtils.encryptBySpliceUserInfo(userDO.getAccount(), roleDO.getName));
    }
    return ResponseServer.error("登录失败，账号或密码错误");
}
```

# 参考项目

https://gitee.com/ok-cloud/ji-spring-boot-starter/tree/demo

# 未来规划

- 依赖上传到 Maven 中央仓库；
- 在依赖外部最小的情况下，引入更多场景。

如果有任何想法或意见，欢迎 issue 或 pr，QQ 交流群：956194623