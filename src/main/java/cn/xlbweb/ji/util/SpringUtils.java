package cn.xlbweb.ji.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * Spring上下文工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Component
public class SpringUtils implements BeanFactoryPostProcessor {

    private static final Log logger = LogFactory.getLog(SpringUtils.class);

    private static ConfigurableListableBeanFactory beanFactory;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        SpringUtils.beanFactory = configurableListableBeanFactory;
    }

    /**
     * 根据Bean类型获取上下文bean
     *
     * @param clazz 指定Bean
     * @param <T>   T
     * @return 上下文Bean
     */
    public static <T> T getBean(Class<T> clazz) {
        return beanFactory.getBean(clazz);
    }
}
