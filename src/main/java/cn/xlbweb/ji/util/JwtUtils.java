package cn.xlbweb.ji.util;

import cn.xlbweb.ji.interceptor.JiProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * jwt工具类
 *
 * @author: wudibo
 * @since 1.0.0
 */
public class JwtUtils {

    private final static Log logger = LogFactory.getLog(JwtUtils.class);

    private static JiProperties jiProperties = SpringUtils.getBean(JiProperties.class);

    private static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    private final static String SEPARATOR = ":";

    // ------------------------------------ 加密 ------------------------------------ \\

    public static String encrypt(String subject) {
        // 当前时间+过期时间
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(jiProperties.getTokenExpirationTime());
        Date expirationTime = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return Jwts.builder().setSubject(subject).setExpiration(expirationTime).signWith(key).compact();
    }

    /**
     * 拼接规则 -> account:role
     * eg1、jack:admin
     * eg2、rose:manager
     *
     * @param account
     * @param role
     * @return
     */
    public static String encrypt(String account, String role) {
        String userInfo = StringUtils.join(account, SEPARATOR, role);
        return encrypt(userInfo);
    }

    // ------------------------------------ 解密 ------------------------------------ \\

    public static Claims decrypt() {
        return decrypt(null);
    }

    public static Claims decrypt(String token) {
        if (StringUtils.isBlank(token)) {
            // 从当前请求头里获取token信息
            token = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader(jiProperties.getTokenName());
        }
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }

    // ------------------------------------ 获取过期时间 ------------------------------------ \\

    public static Date getExpirationTime() {
        return getExpirationTime(null);
    }

    public static Date getExpirationTime(String token) {
        return decrypt(token).getExpiration();
    }

    // ------------------------------------ 获取Subject ------------------------------------ \\

    public static String getSubject() {
        return getSubject(null);
    }

    public static String getSubject(String token) {
        return decrypt(token).getSubject();
    }

    // ------------------------------------ 获取账号&角色 ------------------------------------ \\

    public static String getAccount() {
        String[] userInfos = StringUtils.split(getSubject(), SEPARATOR);
        return userInfos[0];
    }

    public static String getRole() {
        String[] userInfos = StringUtils.split(getSubject(), SEPARATOR);
        if (userInfos.length > 1) {
            return userInfos[1];
        }
        return StringUtils.EMPTY;
    }
}
