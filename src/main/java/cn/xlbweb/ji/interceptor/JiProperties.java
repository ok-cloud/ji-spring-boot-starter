package cn.xlbweb.ji.interceptor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义属性
 *
 * @author: wudibo
 * @since 1.0.0
 */
@Configuration
@ConfigurationProperties("cn.xlbweb.ji")
public class JiProperties {

    /**
     * 是否开启JI组件
     */
    private Boolean enabled = false;

    /**
     * 白名单请求
     */
    private String excludeUris = "/**";

    /**
     * 前后端交互的token名称
     */
    private String tokenName = "JiToken";

    /**
     * token失效时间（单位：分钟）
     */
    private Long tokenExpirationTime = 60L;

    /**
     * token为空的响应码
     */
    private Integer tokenNotBlankCode = 1001;

    /**
     * token为空的响应信息
     */
    private String tokenNotBlankMessage = "Token不能为空";

    /**
     * token过期的响应码
     */
    private Integer tokenExpiredCode = 1002;

    /**
     * token过期的响应信息
     */
    private String tokenExpiredMessage = "Token已过期";

    /**
     * token验证失败的响应码
     */
    private Integer tokenInvalidCode = 1003;

    /**
     * token验证失败的响应信息
     */
    private String tokenInvalidMessage = "Token验证失败";

    /**
     * 用户无权限的响应码
     */
    private Integer noPermissionCode = 1004;

    /**
     * 用户无权限的响应信息
     */
    private String noPermissionMessage = "用户无权限";

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getExcludeUris() {
        return excludeUris;
    }

    public void setExcludeUris(String excludeUris) {
        this.excludeUris = excludeUris;
    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public Long getTokenExpirationTime() {
        return tokenExpirationTime;
    }

    public void setTokenExpirationTime(Long tokenExpirationTime) {
        this.tokenExpirationTime = tokenExpirationTime;
    }

    public Integer getTokenNotBlankCode() {
        return tokenNotBlankCode;
    }

    public void setTokenNotBlankCode(Integer tokenNotBlankCode) {
        this.tokenNotBlankCode = tokenNotBlankCode;
    }

    public String getTokenNotBlankMessage() {
        return tokenNotBlankMessage;
    }

    public void setTokenNotBlankMessage(String tokenNotBlankMessage) {
        this.tokenNotBlankMessage = tokenNotBlankMessage;
    }

    public Integer getTokenExpiredCode() {
        return tokenExpiredCode;
    }

    public void setTokenExpiredCode(Integer tokenExpiredCode) {
        this.tokenExpiredCode = tokenExpiredCode;
    }

    public String getTokenExpiredMessage() {
        return tokenExpiredMessage;
    }

    public void setTokenExpiredMessage(String tokenExpiredMessage) {
        this.tokenExpiredMessage = tokenExpiredMessage;
    }

    public Integer getTokenInvalidCode() {
        return tokenInvalidCode;
    }

    public void setTokenInvalidCode(Integer tokenInvalidCode) {
        this.tokenInvalidCode = tokenInvalidCode;
    }

    public String getTokenInvalidMessage() {
        return tokenInvalidMessage;
    }

    public void setTokenInvalidMessage(String tokenInvalidMessage) {
        this.tokenInvalidMessage = tokenInvalidMessage;
    }

    public Integer getNoPermissionCode() {
        return noPermissionCode;
    }

    public void setNoPermissionCode(Integer noPermissionCode) {
        this.noPermissionCode = noPermissionCode;
    }

    public String getNoPermissionMessage() {
        return noPermissionMessage;
    }

    public void setNoPermissionMessage(String noPermissionMessage) {
        this.noPermissionMessage = noPermissionMessage;
    }
}
