package cn.xlbweb.ji.interceptor;

import cn.xlbweb.ji.constant.JiConstant;
import cn.xlbweb.ji.annotation.RequiresAdmin;
import cn.xlbweb.ji.annotation.RequiresManager;
import cn.xlbweb.ji.response.ServerResponse;
import cn.xlbweb.ji.util.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * jwt拦截器
 *
 * @author: wudibo
 * @since 1.0.0
 */
@Component
public class JiInterceptor implements HandlerInterceptor {

    private final Log logger = LogFactory.getLog(JiInterceptor.class);

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private JiProperties jiProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String token = request.getHeader(jiProperties.getTokenName());
        String uri = request.getRequestURI();
        // 校验token是否为空
        if (StringUtils.isBlank(token)) {
            logger.error("拦截请求[" + uri + "]" + jiProperties.getTokenNotBlankMessage());
            outputJson(response, ServerResponse.error(jiProperties.getTokenNotBlankCode(), jiProperties.getTokenNotBlankMessage()));
            return false;
        }

        // 校验token的有效性
        try {
            JwtUtils.decrypt();
        } catch (ExpiredJwtException e) {
            logger.error("拦截请求[" + uri + "]" + jiProperties.getTokenExpiredMessage(), e);
            ServerResponse serverResponse = ServerResponse.error(jiProperties.getTokenExpiredCode(), jiProperties.getTokenExpiredMessage());
            outputJson(response, serverResponse);
            return false;
        } catch (JwtException e) {
            logger.error("拦截请求[" + uri + "]" + jiProperties.getTokenInvalidMessage(), e);
            ServerResponse serverResponse = ServerResponse.error(jiProperties.getTokenInvalidCode(), jiProperties.getTokenInvalidMessage());
            outputJson(response, serverResponse);
            return false;
        }

        // 校验注解角色
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        RequiresAdmin requiresAdmin = handlerMethod.getMethod().getDeclaredAnnotation(RequiresAdmin.class);
        if (Objects.nonNull(requiresAdmin)) {
            if (StringUtils.equalsIgnoreCase(JwtUtils.getRole(), JiConstant.ADMIN)) {
                return true;
            } else {
                logger.error("非超级管理员，无权操作");
                ServerResponse<Object> serverResponse = ServerResponse.error(jiProperties.getNoPermissionCode(), jiProperties.getNoPermissionMessage());
                outputJson(response, serverResponse);
                return false;
            }
        }

        RequiresManager requiresManager = handlerMethod.getMethod().getDeclaredAnnotation(RequiresManager.class);
        if (Objects.nonNull(requiresManager)) {
            if (StringUtils.equalsIgnoreCase(JwtUtils.getRole(), JiConstant.ADMIN) || StringUtils.equalsIgnoreCase(JwtUtils.getRole(), JiConstant.MANAGER)) {
                return true;
            } else {
                logger.error("非普通管理员，无权操作");
                ServerResponse<Object> serverResponse = ServerResponse.error(jiProperties.getNoPermissionCode(), jiProperties.getNoPermissionMessage());
                outputJson(response, serverResponse);
                return false;
            }
        }
        return true;
    }

    private void outputJson(HttpServletResponse response, Object data) {
        PrintWriter writer = null;
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(objectMapper.writeValueAsString(data));
        } catch (IOException e) {
            logger.error("print response error :", e);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
