package cn.xlbweb.ji.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * 服务端响应
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServerResponse<T> implements Serializable {

    private int code;
    private String message;
    private T data;

    private ServerResponse(int code) {
        this.code = code;
    }

    private ServerResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private ServerResponse(int code, T data) {
        this.code = code;
        this.data = data;
    }

    private ServerResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return this.code == ServerResponseEnum.SUCCESS.getCode();
    }

    // -------------------------------------------------------------------------------- \\

    public static <T> ServerResponse<T> success() {
        return new ServerResponse<>(ServerResponseEnum.SUCCESS.getCode(), ServerResponseEnum.SUCCESS.getMessage());
    }

    public static <T> ServerResponse<T> success(String message) {
        return new ServerResponse<>(ServerResponseEnum.SUCCESS.getCode(), message);
    }

    public static <T> ServerResponse<T> success(T data) {
        return new ServerResponse<>(ServerResponseEnum.SUCCESS.getCode(), ServerResponseEnum.SUCCESS.getMessage(), data);
    }

    public static <T> ServerResponse<T> success(String message, T data) {
        return new ServerResponse<>(ServerResponseEnum.SUCCESS.getCode(), message, data);
    }

    // -------------------------------------------------------------------------------- \\

    public static <T> ServerResponse<T> error() {
        return new ServerResponse<>(ServerResponseEnum.ERROR.getCode(), ServerResponseEnum.ERROR.getMessage());
    }

    public static <T> ServerResponse<T> error(String message) {
        return new ServerResponse<>(ServerResponseEnum.ERROR.getCode(), message);
    }

    public static <T> ServerResponse<T> error(T data) {
        return new ServerResponse<>(ServerResponseEnum.ERROR.getCode(), data);
    }

    public static <T> ServerResponse<T> error(String message, T data) {
        return new ServerResponse<>(ServerResponseEnum.ERROR.getCode(), message, data);
    }

    public static <T> ServerResponse<T> error(int code) {
        return new ServerResponse<>(code, ServerResponseEnum.ERROR.getMessage());
    }

    public static <T> ServerResponse<T> error(int code, T data) {
        return new ServerResponse<>(code, ServerResponseEnum.ERROR.getMessage(), data);
    }

    public static <T> ServerResponse<T> error(int code, String message) {
        return new ServerResponse<>(code, message);
    }

    public static <T> ServerResponse<T> error(int code, String message, T data) {
        return new ServerResponse<>(code, message, data);
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    // -------------------------------------------------------------------------------- \\

    enum ServerResponseEnum {
        SUCCESS(0, "操作成功"),
        ERROR(1, "操作失败");

        private final int code;
        private final String message;

        ServerResponseEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
